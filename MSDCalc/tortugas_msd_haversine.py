import csv
from math import sqrt
import matplotlib.pyplot as plt
import haversine as hs
from haversine import Unit

f = open('/home/laila/PycharmProjects/tortugas/trayectorias_gps/T171_GPSout_D_03dic_2020.csv', 'rt')
rows = csv.reader(f)
headers = next(rows)
msd_temporal = []
msd = []
archivoDestino = open("msd.csv","w")

for i, line in enumerate(f):
    try:
        lat = float(line.split(';')[0])
        lon = float(line.split(';')[1])
        loc1=(lat,lon)
        date = str(line.split(';')[2])
        time = str(line.split(';')[3])
        if (i == 0):
            origen_lat = lat
            origen_lon = lon
            loc2=(origen_lat,origen_lon)
        #hs.haversine(loc1,loc2)
        # To calculate distance in meters
        diff=hs.haversine(loc1, loc2, unit=Unit.METERS)
        #diff = sqrt((lon - origen_lon)**2 + (lat - origen_lat)**2)
        #Sólo si diff cumple cierta condición, se agrega a la lista de msd el nuevo valor.
        msd.append(diff)
        archivoDestino.write(str(diff) + "\n")
    except ValueError:
        print('Faltan datos en la línea', i, 'del archivo.')
#plt.plot(msd)
#plt.figure()
#plt.loglog(msd)
#plt.ylabel('MSD T10')
#plt.show()
archivoDestino.close()
